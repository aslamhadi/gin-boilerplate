package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.kumparan.com/yowez/foxx-redis/routes"
)

func main() {
	router := gin.Default()
	routes.InitializeRoutes(router)
	router.Run() // listen and serve on 0.0.0.0:8080
}
