package routes

import (
	"github.com/gin-gonic/gin"
	"gitlab.kumparan.com/yowez/foxx-redis/domain/news"
)

func InitializeRoutes(router *gin.Engine) {
	v1 := router.Group("/api/v1")
	{
		v1.POST("/rnews", news.SetRedisNews)
	}
}
